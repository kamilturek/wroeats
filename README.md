[![pipeline status](https://gitlab.com/kamilturek/wroeats/badges/master/pipeline.svg)](https://gitlab.com/kamilturek/wroeats/commits/master)

# Prerequisites
- Python 3.6
- Pipenv
- Angular CLI 8 / Node.js 13
- Docker
- Docker Compose
# How to run?
Confidential keys have to be loaded as environment variables.

## Development
### Database
```
$ docker-compose up -d db
```
### Backend
```
$ cd backend
$ pipenv install --dev
$ pipenv shell
$ python manage.py runserver
```
### Frontend
```
$ cd frontend
$ npm install
$ ng serve
```

## Production
```
$ docker-compose up -d --build
```

# Restore DB
```
$ python backend/manage.py restoredb 
```