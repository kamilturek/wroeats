from django.urls import path
from auth.views import CustomObtainAuthToken

urlpatterns = [
    path('', CustomObtainAuthToken.as_view()),
]
