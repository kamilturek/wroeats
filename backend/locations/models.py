from django.db import models


class Location(models.Model):

    street_address = models.CharField(max_length=255)
    postal_code = models.CharField(max_length=6)
    city = models.CharField(max_length=255)

    def __str__(self):
        return self.street_address

    class Meta:
        unique_together = ['street_address', 'postal_code', 'city']
