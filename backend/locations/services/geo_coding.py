# flake8: noqa
import os
import requests


class GeoCodingService:

    def __init__(self):
        self.api_key = os.environ['API_KEY']

    def get_coordinates(self, address):
        response = requests.get(fr'https://geocoder.ls.hereapi.com/6.2/geocode.json?apiKey={self.api_key}&searchtext={address}')
        if response.status_code != 200:
            raise ValueError(address)

        position = response.json()['Response']['View'][0]['Result'][0]['Location']['NavigationPosition'][0]
        lat = position['Latitude']
        lon = position['Longitude']
        return lat, lon

    def get_distance(self, coords1, coords2):
        lat0, lon0 = coords1
        lat1, lon1 = coords2
        response = requests.get(fr'https://route.ls.hereapi.com/routing/7.2/calculateroute.json?apiKey={self.api_key}&waypoint0=geo!{lat0},{lon0}&waypoint1=geo!{lat1},{lon1}&mode=fastest;car;traffic:disabled')
        if response.status_code != 200:
            raise ValueError(coords1, coords2)
        distance = response.json()['response']['route'][0]['summary']['distance']
        return distance
