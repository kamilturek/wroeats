from django.contrib import admin
from .models import Order, Discount, OrderDetail


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'value')


admin.site.register(Order, OrderAdmin)


class DiscountAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'value', 'valid')


admin.site.register(Discount, DiscountAdmin)


class OrderDetailAdmin(admin.ModelAdmin):
    list_display = ('id', 'quantity')


admin.site.register(OrderDetail, OrderDetailAdmin)
