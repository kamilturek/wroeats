from django.db import models

from users.models import CustomUser


class OrderManager(models.Manager):

    def permitted(self, user: CustomUser):
        return super().get_queryset().filter(user=user)
