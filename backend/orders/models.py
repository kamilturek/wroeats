from django.db import models, IntegrityError
from django.db.models import Sum, ExpressionWrapper, DecimalField, F
from django.core.validators import MinValueValidator

from users.models import CustomUser
from restaurants.models import Restaurant, Product
from orders.managers import OrderManager


class Discount(models.Model):

    name = models.CharField(max_length=20)
    valid = models.BooleanField()
    value = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return f'{self.name} {self.value}'


class Order(models.Model):

    PAYPAL = 'PP'
    BLIK = 'BL'
    CREDIT_CARD = 'CC'
    CASH = 'CS'

    PAYMENT_METHODS = [
        (PAYPAL, 'PayPal'),
        (BLIK, 'Blik'),
        (CREDIT_CARD, 'Credit Card'),
        (CASH, 'Cash')
    ]

    payment_method = models.CharField(
        max_length=2,
        choices=PAYMENT_METHODS
    )
    user = models.ForeignKey(
        CustomUser,
        null=True,
        on_delete=models.SET_NULL
    )
    restaurant = models.ForeignKey(
        Restaurant,
        null=True,
        on_delete=models.SET_NULL
    )
    discount = models.ForeignKey(
        Discount,
        null=True,
        on_delete=models.SET_NULL
    )

    @property
    def value(self):
        return self.details.annotate(
            value=ExpressionWrapper(
                F('product__price') * F('quantity'),
                output_field=DecimalField()
            )
        ).aggregate(Sum('value'))['value__sum']

    def __str__(self):
        return str(self.id)

    objects = OrderManager()


class OrderDetail(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='details')
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    quantity = models.IntegerField(validators=[MinValueValidator(1)])

    def save(self, *args, **kwargs):
        if self.product.restaurant != self.order.restaurant:
            raise IntegrityError('One order instance cannot have products from different restaurants.')
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.product} x{self.quantity}'
