from rest_framework import serializers
from django.db import transaction

from users.models import CustomUser
from orders.models import Order, OrderDetail
from restaurants.models import Product
from restaurants.serializers import ProductSerializer


class OrderDetailSerializer(serializers.ModelSerializer):

    product = ProductSerializer(read_only=True)
    product_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = OrderDetail
        exclude = ['order']


class OrderSerializer(serializers.ModelSerializer):

    id = serializers.IntegerField(read_only=True)
    value = serializers.ReadOnlyField()
    details = OrderDetailSerializer(many=True)
    user = serializers.IntegerField(write_only=True)

    @transaction.atomic
    def create(self, validated_data):
        user = CustomUser.objects.get(id=validated_data['user'])
        order = Order.objects.create(
            user=user,
            payment_method=validated_data['payment_method'],
            restaurant=validated_data['restaurant']
        )
        for detail in validated_data['details']:
            product = Product.objects.get(id=detail['product_id'])
            OrderDetail.objects.create(
                order=order,
                product=product,
                quantity=detail['quantity']
            )
        return order

    class Meta:
        model = Order
        fields = ['id', 'value', 'payment_method', 'restaurant', 'details', 'user']
