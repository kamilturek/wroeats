from django.db import IntegrityError
from django.test import TestCase

from users.models import CustomUser
from orders.models import Order, OrderDetail
from restaurants.models import Restaurant, Product
from locations.models import Location


class OrderDetailTest(TestCase):

    def setUp(self):
        location = Location.objects.create(
            street_address='8 Graveyard St.',
            postal_code='88312',
            city='NYC'
        )
        self.user = CustomUser.objects.create_user(
            email='user@user.com',
            password='password',
            first_name='User',
            last_name='User',
            phone_number='1' * 9,
            location=location
        )
        self.restaurant_1 = Restaurant.objects.create(
            name='R1',
            cuisine='C1',
            open_time='11:00',
            close_time='12:00',
            location=location
        )
        self.restaurant_2 = Restaurant.objects.create(
            name='R2',
            cuisine='C2',
            open_time='11:00',
            close_time='12:00',
            location=location
        )
        self.order = Order.objects.create(
            payment_method='PP',
            user=self.user,
            restaurant=self.restaurant_1,
        )

    def test_products_from_different_restaurants(self):
        product_1 = Product.objects.create(
            name='P1',
            price=10.00,
            description='',
            restaurant=self.restaurant_1
        )
        product_2 = Product.objects.create(
            name='P2',
            price=12.00,
            description='',
            restaurant=self.restaurant_2
        )

        OrderDetail.objects.create(
            order=self.order,
            product=product_1,
            quantity=1
        )
        with self.assertRaises(IntegrityError):
            OrderDetail.objects.create(
                order=self.order,
                product=product_2,
                quantity=1
            )
