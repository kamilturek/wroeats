from django.contrib import admin
from .models import Restaurant, Opinion, Product


class RestaurantAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class OpinionAdmin(admin.ModelAdmin):
    list_display = ('id', 'rate', 'user')


class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'description', 'price')


admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(Opinion, OpinionAdmin)
admin.site.register(Product, ProductAdmin)
