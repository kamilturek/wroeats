from django.apps import AppConfig


class RestaurantsConfig(AppConfig):
    name = 'restaurants'


class OpinionsConfig(AppConfig):
    name = 'opinions'


class ProductsConfig(AppConfig):
    name = 'products'
