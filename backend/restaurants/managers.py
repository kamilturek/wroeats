from django.db import models
from users.models import CustomUser


class OpinionManager(models.Manager):

    def permitted(self, user: CustomUser):
        return super().get_queryset().filter(user=user)
