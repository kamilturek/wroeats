# Generated by Django 3.0.2 on 2020-01-11 21:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('restaurants', '0004_auto_20191208_1459'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='restaurant',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='products', to='restaurants.Restaurant'),
        ),
    ]
