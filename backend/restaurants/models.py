from django.db import models, IntegrityError
from django.core.validators import MaxValueValidator, MinValueValidator
from locations.models import Location
from users.models import CustomUser
from restaurants.managers import OpinionManager


class Restaurant(models.Model):
    name = models.CharField(max_length=255)
    cuisine = models.CharField(max_length=255)
    open_time = models.TimeField(auto_now=False, auto_now_add=False)
    close_time = models.TimeField(auto_now=False, auto_now_add=False)
    location = models.ForeignKey(Location, on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class Opinion(models.Model):
    rate = models.DecimalField(max_digits=1, decimal_places=0, validators=[
        MaxValueValidator(5),
        MinValueValidator(0)
    ])
    description = models.CharField(max_length=255, blank=True)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    objects = OpinionManager()

    def save(self, *args, **kwargs):
        if Opinion.objects.filter(user=self.user, restaurant=self.restaurant).exists():
            raise IntegrityError
        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.user} {self.rate}'


class Product(models.Model):
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=5, decimal_places=2, validators=[
        MinValueValidator(0.00),
    ])
    description = models.TextField(max_length=255)
    restaurant = models.ForeignKey(
        Restaurant,
        null=True,
        on_delete=models.SET_NULL,
        related_name='products'
    )

    def __str__(self):
        return self.description
