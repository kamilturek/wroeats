from rest_framework import serializers

from restaurants.models import Restaurant, Opinion, Product


class RestaurantListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Restaurant
        fields = '__all__'


class RestaurantDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Restaurant
        fields = ['id', 'name', 'cuisine', 'open_time', 'close_time', 'location', 'products']
        depth = 1


class OpinionSerializer(serializers.ModelSerializer):

    username = serializers.CharField(source='user.first_name', read_only=True)

    class Meta:
        model = Opinion
        fields = ['rate', 'description', 'user', 'restaurant', 'username']


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = '__all__'
