from django.test import TestCase
from django.db.models.deletion import ProtectedError

from datetime import time
from locations.models import Location
from restaurants.models import Restaurant


class TestRestaurantModel(TestCase):

    def test_deleting_location_raises_protected_error(self):
        location = Location.objects.create(
            street_address='Plac Grunwaldzki 18',
            postal_code='50-384',
            city='Wrocław'
        )
        Restaurant.objects.create(
            name='Pizzeria Bravo',
            cuisine='Italian',
            open_time=time(11, 00),
            close_time=time(23, 00),
            location=location
        )

        with self.assertRaises(ProtectedError):
            location.delete()
