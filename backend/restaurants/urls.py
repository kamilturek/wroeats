from django.urls import path
from restaurants.views import RestaurantList, RestaurantDetail, RestaurantOpinionView

urlpatterns = [
    path('', RestaurantList.as_view()),
    path('<str:address>', RestaurantList.as_view()),
    path('detail/<int:pk>', RestaurantDetail.as_view()),
    path('opinions/<int:pk>', RestaurantOpinionView.as_view())
]
