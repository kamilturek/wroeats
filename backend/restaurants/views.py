from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticated


from django.http import Http404

from restaurants.models import Restaurant, Opinion, Product
from restaurants.serializers import (RestaurantListSerializer,
                                     RestaurantDetailSerializer,
                                     OpinionSerializer,
                                     ProductSerializer)
from locations.services.geo_coding import GeoCodingService


class RestaurantList(APIView):

    def get(self, request, address=None):
        if address:
            try:
                restaurants = self._get_nearby_restaurants(address)
            except ValueError:
                return Response({'detail': 'Could not found given location.'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            restaurants = Restaurant.objects.all()

        serializer = RestaurantListSerializer(restaurants, many=True)
        return Response(serializer.data)

    def _get_nearby_restaurants(self, address, max_distance=20000):
        geo_srv = GeoCodingService()

        address_coords = geo_srv.get_coordinates(address)
        nearby_restaurants = []

        for restaurant in Restaurant.objects.all():
            restaurant_coords = geo_srv.get_coordinates(
                f'{restaurant.location.street_address} {restaurant.location.city}'
            )
            distance = geo_srv.get_distance(address_coords, restaurant_coords)
            if distance < max_distance:
                nearby_restaurants.append(restaurant)

        return nearby_restaurants


class RestaurantDetail(APIView):

    def get_object(self, pk):
        try:
            return Restaurant.objects.get(pk=pk)
        except Restaurant.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        restaurant = self.get_object(pk)
        serializer = RestaurantDetailSerializer(restaurant)
        return Response(serializer.data)


class RestaurantOpinionView(ListAPIView):
    serializer_class = OpinionSerializer

    def get_queryset(self):
        return Opinion.objects.filter(restaurant=self.kwargs['pk'])


class UserOpinionView(ListCreateAPIView):
    serializer_class = OpinionSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Opinion.objects.permitted(self.request.user)


class ProductList(APIView):

    def get(self, request, format=None):
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)
