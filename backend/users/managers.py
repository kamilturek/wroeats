from typing import Dict, Any
from django.contrib.auth.base_user import BaseUserManager


class CustomUserManager(BaseUserManager):
    def create_user(self,
                    email: str,
                    password: str,
                    first_name: str,
                    last_name: str,
                    phone_number: str,
                    location: Dict[str, str] = None,
                    **extra_fields: Any):
        if not email or not password:
            raise ValueError('Email and password must be set.')

        email = self.normalize_email(email)
        user = self.model(
            email=email,
            first_name=first_name,
            last_name=last_name,
            phone_number=phone_number,
            location=location,
            **extra_fields
        )
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        return self.create_user(
            email=email,
            password=password,
            first_name='',
            last_name='',
            phone_number='',
            is_staff=True,
            is_superuser=True,
            is_active=True
        )
