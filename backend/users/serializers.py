from rest_framework import serializers

from users.models import CustomUser
from locations.models import Location
from locations.serializers import LocationSerializer


class UserRegisterSerializer(serializers.ModelSerializer):

    password = serializers.CharField(style={'input_type': 'password'})
    street_address = serializers.CharField(write_only=True)
    city = serializers.CharField(write_only=True)
    postal_code = serializers.CharField(write_only=True)
    location = LocationSerializer(read_only=True)

    class Meta:
        model = CustomUser
        fields = [
            'email',
            'password',
            'first_name',
            'last_name',
            'phone_number',
            'street_address',
            'city',
            'postal_code',
            'location'
        ]

    def save(self):
        location = self.get_location(
            street_address=self.validated_data.pop('street_address'),
            city=self.validated_data.pop('city'),
            postal_code=self.validated_data.pop('postal_code'),
        )
        return CustomUser.objects.create_user(
            email=self.validated_data['email'],
            password=self.validated_data['password'],
            first_name=self.validated_data['first_name'],
            last_name=self.validated_data['last_name'],
            phone_number=self.validated_data['phone_number'],
            location=location
        )

    def get_location(self, **location: str):
        return Location.objects.filter(**location).first() or Location.objects.create(**location)
