from django.test import TestCase
from rest_framework import status

from users.models import CustomUser
from locations.models import Location


class TestUserRegister(TestCase):

    def test_register_user(self):
        self.assertEqual(0, CustomUser.objects.count())
        self.assertEqual(0, Location.objects.count())

        response = self.client.post(
            '/api/users/register',
            data={
                'email': 'john@doe.com',
                'password': 'his_password',
                'first_name': 'John',
                'last_name': 'Doe',
                'phone_number': '666',
                'street_address': '958 River Ave',
                'city': 'Asheville',
                'postal_code': '28803'
            }
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(1, CustomUser.objects.count())
        self.assertEqual(1, Location.objects.count())

    def test_register_user_with_existing_location(self):
        Location.objects.create(
            street_address='958 River Ave',
            city='Asheville',
            postal_code='28803'
        )

        self.assertEqual(0, CustomUser.objects.count())
        self.assertEqual(1, Location.objects.count())

        response = self.client.post(
            '/api/users/register',
            data={
                'email': 'john@doe.com',
                'password': 'his_password',
                'first_name': 'John',
                'last_name': 'Doe',
                'phone_number': '666',
                'street_address': '958 River Ave',
                'city': 'Asheville',
                'postal_code': '28803'
            }
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(1, CustomUser.objects.count())
        self.assertEqual(1, Location.objects.count())

    def test_register_user_with_invalid_data(self):
        response = self.client.post(
            '/api/users/register',
            data={
                'email': '',
                'password': 'his_password',
                'first_name': 'John',
                'last_name': 'Doe',
                'phone_number': '666',
                'street_address': '958 River Ave',
                'city': 'Asheville',
                'postal_code': '28803'
            }
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_register_user_with_incomplete_data(self):
        response = self.client.post(
            '/api/users/register',
            data={
                'email': 'john@doe.com',
                'password': 'his_password',
                'street_address': '958 River Ave',
                'city': 'Asheville',
                'postal_code': '28803'
            }
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
