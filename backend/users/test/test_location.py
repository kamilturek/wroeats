from django.test import TestCase
from django.db.models.deletion import ProtectedError
from locations.models import Location
from users.models import CustomUser


class TestUserLocation(TestCase):

    def test_deleting_location_raises_protected_error(self):
        location = Location.objects.create(
            street_address='Plac Grunwaldzki 18',
            postal_code='50-384',
            city='Wrocław'
        )
        CustomUser.objects.create(
            email='johndoe@email.com',
            first_name='John',
            last_name='Doe',
            location=location
        )

        with self.assertRaises(ProtectedError):
            location.delete()
