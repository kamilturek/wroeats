from django.test import TestCase
from locations.models import Location
from users.models import CustomUser


class TestUserManager(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.location = Location.objects.create(
            street_address='Plac Grunwaldzki 18',
            postal_code='50-384',
            city='Wrocław'
        )

    def test_create_user(self):
        user = CustomUser.objects.create_user(
            email='john@doe.com',
            password='foo',
            first_name='John',
            last_name='Doe',
            phone_number='666',
            location=self.location
        )

        self.assertEqual(user.email, 'john@doe.com')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

    def test_create_user_with_blank_email(self):
        with self.assertRaises(ValueError):
            CustomUser.objects.create_user(
                email='',
                password='foo',
                first_name='John',
                last_name='Doe',
                phone_number='666',
                location=self.location
            )

    def test_create_user_with_blank_password(self):
        with self.assertRaises(ValueError):
            CustomUser.objects.create_user(
                email='john@doe.com',
                password='',
                first_name='John',
                last_name='Doe',
                phone_number='666',
                location=self.location
            )

    def test_create_superuser(self):
        admin_user = CustomUser.objects.create_superuser(
            email='super@user.com',
            password='foo',
            location=self.location
        )

        self.assertEqual(admin_user.email, 'super@user.com')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
