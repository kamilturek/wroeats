from django.urls import path
from users.views import UserRegisterView
from restaurants.views import UserOpinionView

urlpatterns = [
    path('register', UserRegisterView.as_view()),
    path('opinions', UserOpinionView.as_view())
]
