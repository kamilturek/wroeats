from rest_framework.generics import CreateAPIView
from users.models import CustomUser
from users.serializers import UserRegisterSerializer


class UserRegisterView(CreateAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UserRegisterSerializer
