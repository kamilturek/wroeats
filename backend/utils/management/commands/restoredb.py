from django.core.management.base import BaseCommand
from locations.models import Location
from restaurants.models import Restaurant, Product, Opinion
from orders.models import Order, OrderDetail
from users.models import CustomUser


class Command(BaseCommand):
    help = 'Drops all data from database and populates with the initial one.'

    def handle(self, *args, **options):
        self.print_message('Dropping all data...')
        self._erase_data()
        self.print_message('Creating new objects...')
        self._create_data()
        self.print_message('Done')

    def print_message(self, message):
        CGREEN = '\33[32m'
        CEND = '\033[0m'
        print(CGREEN + message + CEND)

    def _erase_data(self):
        Opinion.objects.all().delete()
        OrderDetail.objects.all().delete()
        Order.objects.all().delete()
        Product.objects.all().delete()
        Restaurant.objects.all().delete()
        CustomUser.objects.all().delete()
        Location.objects.all().delete()

    def _create_data(self):
        self._create_locations()
        self._create_users()
        self._create_restaurants()
        self._create_products()
        self._create_orders()
        self._create_opinions()

    def _create_locations(self):
        self.bravo_location = Location.objects.create(
            street_address='Plac Grunwaldzki 18',
            postal_code='50-384',
            city='Wroclaw'
        )
        self.phoviet_location = Location.objects.create(
            street_address='Ładna 1A',
            postal_code='50-353',
            city='Wroclaw'
        )
        self.ugruzina_location = Location.objects.create(
            street_address='Curie-Skłodowskiej 3a',
            postal_code='50-381',
            city='Wroclaw'
        )
        self.mcdonalds_location = Location.objects.create(
            street_address='Plac Grunwaldzki 22',
            postal_code='50-363',
            city='Wroclaw'
        )
        self.user_location = Location.objects.create(
            street_address='Plac Grunwaldzki 16',
            postal_code='50-384',
            city='Wroclaw'
        )

    def _create_users(self):
        CustomUser.objects.create_superuser(
            email='admin@wroeats.com',
            password='admin',
        )
        self.user = CustomUser.objects.create_user(
            email='john@doe.com',
            password='password',
            first_name='John',
            last_name='Doe',
            phone_number='111222333',
            location=self.user_location
        )

    def _create_restaurants(self):
        self.bravo = Restaurant.objects.create(
            name='Pizzeria Bravo',
            cuisine='Italian',
            open_time='11:00',
            close_time='23:00',
            location=self.bravo_location
        )
        self.phoviet = Restaurant.objects.create(
            name='Pho Viet',
            cuisine='Asian',
            open_time='10:00',
            close_time='20:00',
            location=self.phoviet_location
        )
        Restaurant.objects.create(
            name='U Gruzina',
            cuisine='Georgian',
            open_time='11:00',
            close_time='21:00',
            location=self.ugruzina_location
        )
        Restaurant.objects.create(
            name='McDonald\'s',
            cuisine='Fast-Food',
            open_time='09:00',
            close_time='23:00',
            location=self.mcdonalds_location
        )

    def _create_products(self):
        # BRAVO
        self.product_1 = Product.objects.create(
            name='Margherita',
            price=12.90,
            description='sos, ser',
            restaurant=self.bravo
        )
        self.product_2 = Product.objects.create(
            name='Chleb czosnkowy',
            price=12.90,
            description='czosnek, ser',
            restaurant=self.bravo
        )
        self.product_3 = Product.objects.create(
            name='Funghi',
            price=14.90,
            description='sos pomidorowy, ser, pieczarki',
            restaurant=self.bravo
        )
        Product.objects.create(
            name='Pomodoro',
            price=14.90,
            description='sos, ser, pomidor',
            restaurant=self.bravo
        )
        Product.objects.create(
            name='Neapolitana',
            price=15.70,
            description='sos, ser, czosnek, pomidor',
            restaurant=self.bravo
        )
        Product.objects.create(
            name='Salami',
            price=15.70,
            description='sos, ser, salami',
            restaurant=self.bravo
        )
        Product.objects.create(
            name='Capri',
            price=15.70,
            description='sos, ser, szynka',
            restaurant=self.bravo
        )
        Product.objects.create(
            name='Hawaian',
            price=17.00,
            description='sos, ser, szynka, ananas',
            restaurant=self.bravo
        )
        Product.objects.create(
            name='Roma',
            price=17.00,
            description='sos, ser, szynka, pieczarki',
            restaurant=self.bravo
        )
        Product.objects.create(
            name='Pepperoni',
            price=17.00,
            description='sos, ser, salami',
            restaurant=self.bravo
        )
        Product.objects.create(
            name='Ciuriosita',
            price=18.70,
            description='sos, ser, szynka, papryka, szpinak',
            restaurant=self.bravo
        )
        Product.objects.create(
            name='Classic',
            price=18.70,
            description='sos, ser, piezcarki, salami, papryka pepperoni',
            restaurant=self.bravo
        )
        Product.objects.create(
            name='Verona',
            price=18.70,
            description='sos, ser, polędwica, ananas, papryka pepperoni',
            restaurant=self.bravo
        )
        Product.objects.create(
            name='Cosa Nostra',
            price=20.60,
            description='sos, ser, pieczarki, salami, kukurydza, oliwki',
            restaurant=self.bravo
        )

        # PHOVIET
        self.product_4 = Product.objects.create(
            name='Kurczak chrupiący słodko-kwaśny',
            price=15.00,
            restaurant=self.phoviet
        )
        self.product_5 = Product.objects.create(
            name='Kurczak chrupiący na ostro',
            price=15.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Filet chrupiący po tajsku',
            price=15.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Kurczak ostro-słodki',
            price=15.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Kurczak w sosie czosnkowym',
            price=15.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Kurczak w sosie słodko-kwaśnym',
            price=13.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Kurczak w cieście',
            price=13.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Kurczak w pięciu smakach',
            price=13.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Kurczak z warzywami',
            price=13.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Kurczak w sosie curry',
            price=13.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Kurczak po chińsku',
            price=13.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Kurczak z bambusem',
            price=13.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Kurczak z grzybami',
            price=13.00,
            restaurant=self.phoviet
        )
        Product.objects.create(
            name='Kurczak z ananasem',
            price=13.00,
            restaurant=self.phoviet
        )

    def _create_orders(self):
        self.order_1 = Order.objects.create(
            payment_method=Order.PAYPAL,
            user=self.user,
            restaurant=self.bravo,
        )
        OrderDetail.objects.create(
            order=self.order_1,
            product=self.product_1,
            quantity=1
        )
        OrderDetail.objects.create(
            order=self.order_1,
            product=self.product_2,
            quantity=2
        )
        OrderDetail.objects.create(
            order=self.order_1,
            product=self.product_3,
            quantity=3
        )

        self.order_2 = Order.objects.create(
            payment_method=Order.CASH,
            user=self.user,
            restaurant=self.phoviet
        )
        OrderDetail.objects.create(
            order=self.order_2,
            product=self.product_4,
            quantity=1
        )
        OrderDetail.objects.create(
            order=self.order_2,
            product=self.product_5,
            quantity=2
        )

    def _create_opinions(self):
        Opinion.objects.create(
            rate=4,
            description='Very tasty! :)',
            restaurant=self.bravo,
            user=self.user
        )
