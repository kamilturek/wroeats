from .base import *
from .env import export_envs

PROJECT_DIR = os.path.dirname(os.path.dirname(BASE_DIR))

export_envs(os.path.join(PROJECT_DIR, 'wroeats.env'))

DEBUG = True

CORS_ORIGIN_ALLOW_ALL = True

SECRET_KEY = os.environ['SECRET_KEY']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': os.environ['POSTGRES_USER'],
        'PASSWORD': os.environ['POSTGRES_PASSWORD'],
        'HOST': 'localhost',
        'PORT': 8001
    }
}
