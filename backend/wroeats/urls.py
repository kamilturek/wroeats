from django.contrib import admin
from django.urls import path, include

admin.site.site_header = 'Wroeats Administration Panel'
admin.site.site_title = 'Wroeats Administration Panel'
admin.site.index_title = 'Wroeats Administration Panel'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/auth/', include('auth.urls')),
    path('api/restaurants/', include('restaurants.urls')),
    path('api/users/', include('users.urls')),
    path('api/locations/', include('locations.urls')),
    path('api/orders/', include('orders.urls')),
]
