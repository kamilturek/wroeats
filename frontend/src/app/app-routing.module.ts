import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { RestaurantListComponent } from 'src/app/restaurant/restaurant-list/restaurant-list.component';
import { LoginComponent } from 'src/app/auth/login/login.component';
import { MainComponent } from 'src/app/main/main.component';
import { RestaurantDetailComponent } from './restaurant/restaurant-detail/restaurant-detail.component';
import { ShoppingCartGuard } from './shopping-cart/shopping-cart.guard';
import { RegisterComponent } from './auth/register/register.component';
import { OrderListComponent } from './order/order-list/order-list.component';
import { OrderSummaryComponent } from './order/order-summary/order-summary.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, data: {animation: 'LoginPage'} },
  { path: 'register', component: RegisterComponent, data: {animation: 'RegisterPage'} },
  { path: 'restaurants', component: RestaurantListComponent, data: {animation: 'RestaurantListPage'} },
  { path: 'restaurants/:id', component: RestaurantDetailComponent, data: {animation: 'RestaurantDetailPage'} }, //canDeactivate: [ShoppingCartGuard],
  { path: 'orders', component: OrderListComponent, canActivate: [AuthGuard], data: {animation: 'OrderListPage'} },
  { path: 'orders/summary', component: OrderSummaryComponent, canActivate: [AuthGuard], data: {animation: 'OrderSummaryPage'} },
  { path: 'home', component: MainComponent, data: {animation: 'MainPage'} },
  { path: '**', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
