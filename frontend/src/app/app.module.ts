import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { RestaurantListComponent } from './restaurant/restaurant-list/restaurant-list.component';
import { LoginComponent } from './auth/login/login.component';
import { MainComponent } from './main/main.component';
import { SearchRestaurantsComponent } from './main/search-restaurants/search-restaurants.component';
import { FilterBarComponent } from './restaurant/restaurant-list/filter-bar/filter-bar.component';
import { FilterCuisineComponent } from './restaurant/restaurant-list/filter-bar/filter-cuisine/filter-cuisine.component';
import { RestaurantListElementComponent } from './restaurant/restaurant-list/restaurant-list-element/restaurant-list-element.component';
import { ShortTimePipe } from './pipes/short-time.pipe';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { RestaurantDetailComponent } from './restaurant/restaurant-detail/restaurant-detail.component';
import { ProductListElementComponent } from './product/product-list/product-list-element/product-list-element.component';
import { ProductFilterBarComponent } from './product/product-list/product-filter-bar/product-filter-bar.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ShoppingCartCardComponent } from './shopping-cart/shopping-cart-card/shopping-cart-card.component';
import { DialogDiscardShoppingCartComponent } from './shopping-cart/dialog-discard-shopping-cart/dialog-discard-shopping-cart.component';
import { RegisterComponent } from './auth/register/register.component';
import { OrderListComponent } from './order/order-list/order-list.component';
import { OrderListElementComponent } from './order/order-list/order-list-element/order-list-element.component';
import { OrderSummaryComponent } from './order/order-summary/order-summary.component';
import { OrderDialogComponent } from './order/order-dialog/order-dialog.component';
import { OpinionDialogComponent } from './opinion/opinion-dialog/opinion-dialog.component';
import { OpinionListElementComponent } from './opinion/opinion-list-element/opinion-list-element.component';

@NgModule({
  declarations: [
    AppComponent,
    RestaurantListComponent,
    LoginComponent,
    MainComponent,
    SearchRestaurantsComponent,
    FilterBarComponent,
    FilterCuisineComponent,
    RestaurantListElementComponent,
    ShortTimePipe,
    ToolbarComponent,
    RestaurantDetailComponent,
    ProductListElementComponent,
    ProductFilterBarComponent,
    ProductListComponent,
    ShoppingCartCardComponent,
    DialogDiscardShoppingCartComponent,
    RegisterComponent,
    OrderListComponent,
    OrderListElementComponent,
    OrderSummaryComponent,
    OrderDialogComponent,
    OpinionDialogComponent,
    OpinionListElementComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  entryComponents: [
    DialogDiscardShoppingCartComponent,
    OrderDialogComponent,
    OpinionDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
