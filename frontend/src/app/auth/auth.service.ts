import { Injectable } from '@angular/core';
import { BackendService } from '../services/backend.service';
import { NotificationService } from '../services/notification.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private email: string;
  private token: string;
  private userId: number;
  private loggedIn: boolean = false;

  constructor(private backendService: BackendService, private router: Router, private notificationService: NotificationService) {}

  isLoggedIn(): boolean {
    return this.loggedIn;
  }

  loginAsSuperuser() {
    const email = 'john@doe.com';
    const password = 'password';
    this.logIn(email, password);
  }

  getEmail(): string {
    return this.email;
  }

  getToken(): string {
    return this.token;
  }

  getUserId(): number {
    return this.userId;
  }

  logIn(email: string, password: string): void {
    this.backendService.post('/api/auth/', {
      username: email,
      password: password
    }).subscribe(res => {
      this.email = email;
      this.token = res.token;
      this.userId = res.user_id;
      this.loggedIn = true;
      this.router.navigate(['/']);
    }, err => {
      this.notificationService.notifyError(err.error.non_field_errors[0]);
    });
  }

  register(userData): void {
    this.backendService.post('/api/users/register', userData).subscribe(res => {
      this.logIn(userData.email, userData.password);
    }, err => {
      if (err.error.email.length > 0)
        this.notificationService.notifyError('User with this email already exists.');
    });
  }
}
