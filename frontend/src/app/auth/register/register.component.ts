import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  
  registerForm = new FormGroup({
    email: new FormControl('', [Validators.email]),
    password: new FormControl(''),
    first_name: new FormControl('', [Validators.pattern('[A-Za-z]+')]),
    last_name: new FormControl('', [Validators.pattern('[A-Za-z]+')]),
    phone_number: new FormControl('', [Validators.pattern('[0-9-]+')]),
    street_address: new FormControl(''),
    city: new FormControl(''),
    postal_code: new FormControl(''),
  });

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.registerForm.valid)
      this.authService.register(this.registerForm.value);
  }
}
