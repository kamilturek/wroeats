import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { RestaurantService } from 'src/app/services/restaurant.service';

@Component({
  selector: 'app-search-restaurants',
  templateUrl: './search-restaurants.component.html',
  styleUrls: ['./search-restaurants.component.scss']
})
export class SearchRestaurantsComponent implements OnInit {
  searchForm = new FormGroup({
    address: new FormControl(''),
  });

  constructor(private restaurantService: RestaurantService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    if (!this.searchForm.invalid) {
      const currentAddress = this.searchForm.get('address').value;
      this.restaurantService.setCurrentAddress(currentAddress);
      this.router.navigate(['/restaurants']);
    }
  }

}
