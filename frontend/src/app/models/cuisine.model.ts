export class Cuisine {
    name: string;
    checked: boolean = true;

    constructor(name: string) {
        this.name = name;
    }
}