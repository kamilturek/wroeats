export class Product {
    id: number;
    name: string;
    price: number;
    quantity: number;
    restaurant: number;

    constructor(id: number, name: string, price: number, quantity: number, restaurant: number) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.restaurant = restaurant;
    }
}