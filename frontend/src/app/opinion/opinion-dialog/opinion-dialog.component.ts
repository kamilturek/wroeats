import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-opinion-dialog',
  templateUrl: './opinion-dialog.component.html',
  styleUrls: ['./opinion-dialog.component.scss']
})
export class OpinionDialogComponent implements OnInit {

  rate: number = 0;
  description = new FormControl('');

  constructor() { }

  ngOnInit() {
  }

  isRateSet(rate: number) {
    if (this.rate >= rate)
      return 'rateSet';

    return '';
  }

  setRate(rate: number) {
    this.rate = rate;
  }
}
