import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-opinion-list-element',
  templateUrl: './opinion-list-element.component.html',
  styleUrls: ['./opinion-list-element.component.scss']
})
export class OpinionListElementComponent implements OnInit {

  @Input() opinion: any;

  constructor() { }

  ngOnInit() {
  }

  isRateSet(rate: number) {
    if (this.opinion.rate >= rate)
      return 'rate-set';

    return '';
  }
}
