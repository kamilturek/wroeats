import { Component, OnInit, Input } from '@angular/core';
import { RestaurantService } from 'src/app/services/restaurant.service';
import { MatDialog } from '@angular/material/dialog';
import { OpinionDialogComponent } from 'src/app/opinion/opinion-dialog/opinion-dialog.component';
import { OpinionService } from 'src/app/services/opinion.service';

@Component({
  selector: 'app-order-list-element',
  templateUrl: './order-list-element.component.html',
  styleUrls: ['./order-list-element.component.scss']
})
export class OrderListElementComponent implements OnInit {

  @Input() data: any;
  restaurant: any;

  constructor(private restaurantService: RestaurantService,
              private opinionService: OpinionService,
              public dialog: MatDialog) { }

  ngOnInit() {
    if (this.data)
      this.restaurantService.getRestaurant(this.data.restaurant).subscribe(res => this.restaurant = res);
  }

  round(num: number, places: number) {
    return Math.round(num * (10 ** places)) / (10 ** places);
  }

  openOpinionDialog(): void {
    const dialogRef = this.dialog.open(OpinionDialogComponent, {
      width: '500px',
      height: '320px'
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res)
        this.opinionService.sendOpinion(res, this.restaurant);
    });
  }
}
