import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';
import { RestaurantService } from 'src/app/services/restaurant.service';
import { OrderService } from 'src/app/services/order.service';
import { AuthService } from 'src/app/auth/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { OrderDialogComponent } from '../order-dialog/order-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.scss']
})
export class OrderSummaryComponent implements OnInit {

  restaurant: any;
  paymentMethod: string = 'PP';

  constructor(private shoppingCartService: ShoppingCartService,
              private orderService: OrderService,
              private restaurantService: RestaurantService,
              private authService: AuthService,
              private router: Router,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.fetchRestaurant();
  }

  confirmOrder() {
    const userId = this.authService.getUserId();
    this.orderService.postOrder(this.shoppingCartService.products, this.paymentMethod, this.restaurant.id, userId);
    this.shoppingCartService.clear();
    this.openDialog();
  }

  private openDialog(): void {
    const dialogRef = this.dialog.open(OrderDialogComponent, {
      width: '400px',
      height: '220px'
    });

    dialogRef.afterClosed().subscribe(res => {
      this.router.navigateByUrl('/orders');
    });
  }

  private fetchRestaurant(): void {
    const restaurantId = this.shoppingCartService.getRestaurantId();
    this.restaurantService.getRestaurant(restaurantId).subscribe(res => this.restaurant = res);
  }
}
