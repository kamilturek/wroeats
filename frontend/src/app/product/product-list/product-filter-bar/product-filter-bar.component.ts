import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-product-filter-bar',
  templateUrl: './product-filter-bar.component.html',
  styleUrls: ['./product-filter-bar.component.scss']
})
export class ProductFilterBarComponent implements OnInit{
  @Input() minPrice: number;
  @Input() maxPrice: number;
  @Input() currentPrice: number = this.maxPrice;
  @Output() priceFiltered: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  getStep() {
    const step = (this.maxPrice - this.minPrice) / 20;
    return this.round(step, 1);
  }

  round(num: number, places: number) {
    return Math.round(num * (10 ** places)) / (10 ** places);
  }

  sliderChanged(event) {
    const sliderValue = event.value;
    this.filterPrice(sliderValue)
  }

  filterPrice(newPriceValue: number) {
    this.priceFiltered.emit(newPriceValue);
  }
}
