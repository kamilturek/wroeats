import { Component, OnInit, Input } from '@angular/core';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';

@Component({
  selector: 'app-product-list-element',
  templateUrl: './product-list-element.component.html',
  styleUrls: ['./product-list-element.component.scss']
})
export class ProductListElementComponent implements OnInit {

  @Input()
  data: any;

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit() {
  }

  addToCart(): void {
    this.shoppingCartService.addProduct(this.data);
  }  

  removeFromCart(): void {
    this.shoppingCartService.removeProduct(this.data);
  }

}
