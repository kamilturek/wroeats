import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  @Input()
  products: any;

  constructor() { }

  ngOnInit() {
  }

  filterProducts(price: number) {
    this.products.forEach(product => {
      if (product.price > price)
        product.visible = false;
      else
        product.visible = true;
    });
  }

  getPrices() {
    return this.products.map(product => Number(product.price));
  }

  getMinPrice() {
    if (this.products) {
      return Math.min(...this.getPrices());
    }

    return 0;
  }

  getMaxPrice() {
    if (this.products)
      return Math.max(...this.getPrices());
    
    return 0;
  }
}
