import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantService } from 'src/app/services/restaurant.service';
import { NotificationService } from 'src/app/services/notification.service';
import { OpinionService } from 'src/app/services/opinion.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-restaurant-detail',
  templateUrl: './restaurant-detail.component.html',
  styleUrls: ['./restaurant-detail.component.scss']
})
export class RestaurantDetailComponent implements OnInit {

  restaurantData: any;
  opinions$: Observable<any[]>;

  constructor(private route: ActivatedRoute,
              private restaurantService: RestaurantService,
              private notificationService: NotificationService,
              private opinionService: OpinionService) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');

    this.restaurantService.getRestaurant(Number(id)).subscribe(res => {
      this.restaurantData = res;
      this.restaurantData.products.forEach(product => product.visible = true);
      this.opinions$ = this.opinionService.getOpinions(this.restaurantData.id);
    }, err => {
      this.notificationService.notifyError(err.error.detail);
    });
  }

}
