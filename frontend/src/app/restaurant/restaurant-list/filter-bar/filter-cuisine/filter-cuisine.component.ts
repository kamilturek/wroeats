import { Component, OnInit } from '@angular/core';
import { RestaurantService } from 'src/app/services/restaurant.service';

@Component({
  selector: 'app-filter-cuisine',
  templateUrl: './filter-cuisine.component.html',
  styleUrls: ['./filter-cuisine.component.scss']
})
export class FilterCuisineComponent implements OnInit {

  constructor(private restaurantService: RestaurantService) { }

  ngOnInit() {
  }

}
