import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-restaurant-list-element',
  templateUrl: './restaurant-list-element.component.html',
  styleUrls: ['./restaurant-list-element.component.scss']
})
export class RestaurantListElementComponent implements OnInit {

  @Input()
  data: any;

  constructor() { }

  ngOnInit() {
  }

}
