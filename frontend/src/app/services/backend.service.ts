import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  private apiUrl: string = environment.apiUrl;
  private httpOptions = { headers: new HttpHeaders() };

  constructor(private http: HttpClient) { }

  get(endpoint: string, token?: string): Observable<any> {
    if (token)
      this.authorize(token);

    return this.http.get(this.apiUrl + endpoint, this.httpOptions);
  }

  post(endpoint: string, body: any, token?: string): Observable<any> {
    if (token)
      this.authorize(token);

    return this.http.post(this.apiUrl + endpoint, body, this.httpOptions);
  }

  private authorize(token: string) {
    this.httpOptions.headers = this.httpOptions.headers.set('Authorization', `Token ${token}`);
  }
}
