import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';
import { AuthService } from '../auth/auth.service';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class OpinionService {

  constructor(private backendService: BackendService,
              private authService: AuthService,
              private notificationService: NotificationService) { }

  getOpinions(restaurantId: number) {
    return this.backendService.get(`/api/restaurants/opinions/${restaurantId}`);
  }
  
              sendOpinion(opinion, restaurant): void {
    this.backendService.post('/api/users/opinions', {
      rate: opinion.rate,
      description: opinion.description,
      restaurant: restaurant.id,
      user: this.authService.getUserId()
    }, this.authService.getToken()).subscribe(res => {
      this.notificationService.notifyError('Your opinion has been sent. Thank you!');
    }, err => {
      this.notificationService.notifyError('You have already rated this restaurant.');
    });
  }
}
