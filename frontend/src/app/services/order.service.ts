import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private backendService: BackendService, private authService: AuthService) { }

  getOrders() {
    const token = this.authService.getToken();
    return this.backendService.get('/api/orders', token);
  }

  postOrder(products, paymentMethod, restaurantId, userId): void {
    const orderData = {
      payment_method: paymentMethod,
      restaurant: restaurantId,
      user: userId,
      details: []
    };
    products.forEach(product => {
      orderData.details.push({
        quantity: product.quantity,
        product_id: product.id
      })
    });
    this.backendService.post('/api/orders/', orderData, this.authService.getToken()).subscribe(console.log);
  }
}
