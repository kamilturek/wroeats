import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';
import { NotificationService } from './notification.service';
import { Cuisine } from '../models/cuisine.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  restaurants: any[] = [];
  cuisines: Set<Cuisine> = new Set<Cuisine>();
  private currentAddress: string = '';

  constructor(private backendService: BackendService, private notificationService: NotificationService) {
    this.fetchRestaurants();
  }

  setCurrentAddress(value: string) {
    this.currentAddress = value;
    this.fetchRestaurants();
  }

  filterByCuisine(cuisine: Cuisine) {
    this.restaurants.forEach(restaurant => {
      if (restaurant.cuisine == cuisine.name) {
        restaurant.visible = cuisine.checked;
      }
    })
  }

  getRestaurant(id: number) {
    return this.backendService.get(`/api/restaurants/detail/${id}`);
  }

  private fetchRestaurants(): void {
    this.backendService.get(`/api/restaurants/${this.currentAddress}`).subscribe(res => {
      this.restaurants = res;
      this.restaurants.forEach(restaurant => restaurant.visible = true);
      this.fetchCuisines();
    }, err => {
      this.notificationService.notifyError(err.error.detail);
    });
  }

  private fetchCuisines(): void {
    this.cuisines.clear();
    this.restaurants.forEach(restaurant => this.cuisines.add(new Cuisine(restaurant.cuisine)));
  }
}
