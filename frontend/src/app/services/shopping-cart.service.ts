import { Injectable } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { NotificationService } from './notification.service';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  products: Product[] = [];

  constructor(private authService: AuthService, private notificationService: NotificationService) { }

  addProduct(newProduct): void {
    if (!this.authService.isLoggedIn()) {
      this.notificationService.notifyError('Please login first to place the order.');
      return;
    }

    if (this.products.length > 0 && newProduct.restaurant != this.products[0].restaurant) {
      this.notificationService.notifyError('You cannot mix products from different restaurants');
      return;
    }

    let product = this.findProduct(newProduct);
    if (product)
      product.quantity++;
    else
      this.insertProduct(newProduct);
  }

  removeProduct(productToRemove): void {
    if (!this.authService.isLoggedIn()) {
      this.notificationService.notifyError('Please login first to place the order.');
      return;
    }

    let product = this.findProduct(productToRemove);
    if (product) {
      product.quantity--;
      if (product.quantity == 0)
        this.eraseProduct(product);
    }
  }

  getTotalCost() {
    return this.products.reduce((sum, product) => sum + product.price * product.quantity, 0);
  }

  getRestaurantId(): number {
    if (this.products.length > 0)
      return this.products[0].restaurant;

    return undefined;
  }

  empty() {
    return this.products.length == 0;
  }

  clear() {
    while (this.products.length > 0)
      this.products.pop();
  }

  private insertProduct(product: any): void {
    this.products.push(new Product(product.id, product.name, Number(product.price), 1, product.restaurant));
  }

  private eraseProduct(product): void {
    this.products = this.products.filter(item => item.name != product.name);
  }

  private findProduct(product): Product {
    return this.products.find(item => item.name == product.name);
  }
}
