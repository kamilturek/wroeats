import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dialog-discard-shopping-cart',
  templateUrl: './dialog-discard-shopping-cart.component.html',
  styleUrls: ['./dialog-discard-shopping-cart.component.scss']
})
export class DialogDiscardShoppingCartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
