import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { ShoppingCartService } from 'src/app/services/shopping-cart.service';

@Component({
  selector: 'app-shopping-cart-card',
  templateUrl: './shopping-cart-card.component.html',
  styleUrls: ['./shopping-cart-card.component.scss']
})
export class ShoppingCartCardComponent implements OnInit {

  constructor(private authService: AuthService, private shoppingCartService: ShoppingCartService) { }

  ngOnInit(): void {
  }

}
