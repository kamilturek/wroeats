import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { RestaurantDetailComponent } from '../restaurant/restaurant-detail/restaurant-detail.component';
import { ShoppingCartService } from '../services/shopping-cart.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogDiscardShoppingCartComponent } from './dialog-discard-shopping-cart/dialog-discard-shopping-cart.component';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartGuard implements CanDeactivate<RestaurantDetailComponent> {

  private discardSubject: Subject<boolean> = new Subject<boolean>();

  constructor(public dialog: MatDialog, private shoppingCartService: ShoppingCartService) { }

  canDeactivate(
    component: RestaurantDetailComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): boolean | Observable<boolean> {
    if (!this.shoppingCartService.empty() && nextState.url != '/orders/summary')
      return this.openDiscardDialog().afterClosed();

    return true;
  }

  private openDiscardDialog(): MatDialogRef<DialogDiscardShoppingCartComponent> {
    return this.dialog.open(DialogDiscardShoppingCartComponent, {
      height: '200px',
      width: '400px'
    });
  }

}
